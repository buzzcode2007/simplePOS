/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tech.iBeans.POSware.test;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 *
 * @author eleven
 */
public class data_test {
    public static Dictionary<String, Dictionary> fake_items = new Hashtable<>();
    
    private static void fill() {
        /* Fake items. */
        Dictionary item_1 = new Hashtable<>();
        item_1.put("Name", "Plastic Bag");
        item_1.put("Price", (float) 1);
        fake_items.put("13f", item_1);
        
        Dictionary item_2 = new Hashtable<>();
        item_2.put("Name", "Bill Gates");
        item_2.put("Price", (float) 1000000);
        item_2.put("Discount", 0);
        item_2.put("Tax", 0);
        fake_items.put("13a", item_2);
        
        Dictionary item_3 = new Hashtable<>();
        item_3.put("Name", "Microsoft Windows Lisence");
        item_3.put("Price", (float) 100);
        item_3.put("Discount", -10);
        item_3.put("Tax", 20);
        fake_items.put("13d", item_3);
        
        Dictionary item_4 = new Hashtable<>();
        item_4.put("Name", "Microsoft Office 2010 ProfessionalPlus Lisence");
        item_4.put("Price", (float) 50);
        item_4.put("Discount", -10);
        item_4.put("Tax", 20);
        fake_items.put("13c", item_4);
    }
    
    private static Dictionary<String, String> fake_details = new Hashtable<>();
    
    public static Dictionary<String, String> read_properties() {
        /* Spam details. */
        
        fake_details.put("Name", "Contoso");
        fake_details.put("Address", "1 Infinite Loop Cupertino CA 95014 United States");
        fake_details.put("Contact Number", "‭1 (800) MYAPPLE‬");
        fake_details.put("URL", "http://apple.com/");
        
        return fake_details;
    };
    
    static {
        fill();
    };
}
