/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tech.iBeans.POSware.Lite;
import java.util.*;

import tech.iBeans.POSware.test.data_test;

/**
 *
 * @author eleven
 */
public class receipt {
    public static String content = "";
	private static Dictionary<String, String> data = new Hashtable<>();
	public static Boolean clear = true;
    
    public static void init() {
        content = "";
		data.put("headers", "");
		data.put("cart", "");
		data.put("payment", "");
		data.put("footer", "");
    };
	
	public static String create() {
		/* This is different from an initialization or reset — this alsof adds the headers! 
			
			Returns: (String) the content
		*/
			
		init();
		headers();
		
		return(content);
	};
    
	private static String ready() {
		/* Prepare for a new data entry. 
			
			Returns: (String) the updated receipt
		*/
			
		content = content.concat("\n\n");
		
		return(content);
	};
	
	public static String refresh() {
		/* Refresh the receipt's content. 
				Returns: (string) the updated conntent
		*/
		
		content = "";
		
		if (data.get("headers") != null) {
			content = content.concat(data.get("headers").toString());
			ready();
		};
		if (data.get("cart") != null) {
			if (!data.get("cart").isBlank()) {
				clear = false;
				content = content.concat(data.get("cart").toString());
				ready();
			} else {
				clear = true;
			};
		} else {
			clear = true;
		};
		if (data.get("payment") != null) {
			content = content.concat(data.get("payment").toString());
			ready();
		};
		if (data.get("footer") != null) {
			content = content.concat(data.get("footer").toString());
			ready();
		};
		
		return(content);
	};
	
    private static String headers() {
        /* Generate the headers. 
        */
        
       	Dictionary <String, String> content_headers_raw = data_test.read_properties();
        	Enumeration<String> content_headers_raw_headers = content_headers_raw.keys();
        	String content_headers = "";
        
        	if (content_headers_raw.get("Full Name") != null) {
          	content_headers = content_headers_raw.get("Full Name").toUpperCase();
        	} else if (content_headers_raw.get("Name") != null) {
          	content_headers = content_headers_raw.get("Name").toUpperCase();
        	};
        
        	while (content_headers_raw_headers.hasMoreElements()) {
          	String content_headers_header = content_headers_raw_headers.nextElement();
          	if (content_headers_raw.get(content_headers_header) != null) {
               	if (!content_headers_header.contains("Name")) {
                    	content_headers = content_headers.concat("\n");
                    	content_headers = content_headers.concat(content_headers_header.concat(": "));
                    	content_headers = content_headers.concat(content_headers_raw.get(content_headers_header));
                	};
            	};
        	};
        
        
		data.put("headers", content_headers);
		refresh();
        
     	return(content_headers);
    	};
	
	public static String action(String NAME, int QUANTITY, double PRICE, double TOTAL) {
		/* Add an item to the cart. 
			Parameters: 
				(String) NAME: the name of the item
				(int) QUANTITY: the quantity of the items in the cart
				(double) PRICE: the unit price
				(double) TOTAL: the total price
			Returns: (String) the updated list of items in the cart
		*/
		
		if (data.get("cart").toString().isBlank()) {
			data.put("cart", "Quantity\tItem\n\tPrice\tTotal");
		};
		
     	data.put("cart", data.get("cart").toString().concat("\n"));
		String item_current = String.join("\t", String.valueOf(QUANTITY), NAME, "\n", String.valueOf(PRICE), String.valueOf(TOTAL));
		data.put("cart", data.get("cart").toString().concat(item_current));
                
          // Refresh. 
		refresh();
                
          // Return the updated receipt section preview. 
		return(data.get("cart").toString());
	};

	public static String payment(Dictionary<String, Float> details) {
		/* Add the payment data. 
		 * 
		 * Parameters: 
		 * 	(Dict) details: the payment details, including total, subtotal, and change
		 * 
		 * Returns: (String) the portion of the receipt
		*/

		// re-initialize the payment details
		data.put("payment", "");
		
		// Check for the details
		Enumeration<String> details_contents = details.keys();	

		while (details_contents.hasMoreElements()) {
			String details_content = details_contents.nextElement();
			
			data.put("payment", data.get("payment").concat("\n"));
			data.put("payment", data.get("payment").concat(String.join(": ", (details_content.contains("total")) ? details_content.toUpperCase() : details_content.substring(0, 1).toUpperCase() + details_content.substring(1), String.valueOf(details.get(details_content)))));
		};

		refresh();

		return(data.get("payment"));
	};

	private static String footer() {
		data.put("footer", "Thank you for shopping with us. Have a good day. "); // POC. must replace
		refresh();

		return(data.get("footer"));
	}

	public static String finish() {
		footer();
		refresh();
		return(content);
	}
    
}
