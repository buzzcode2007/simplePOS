/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tech.iBeans.POSware.Lite;
import java.util.*;

import tech.iBeans.POSware.test.data_test;

/**
 *
 * @author eleven
 */
public class inventory {
    
    public static Dictionary<String, Dictionary> items = new Hashtable<>();
    public static ArrayList<String> item_names = collate();
    
    public static boolean refresh() {
        /* Refresh the inventory. 
        
        Returns: (bool) the refresh state
        */
        // Pull the sample items. 
        items = data_test.fake_items;
        item_names = collate();
        return (true);
    };
    
    
    public static Object get(String SKU, String PROPERTY) {
        /* Get the information of a particular SKU. 
        
        Parameters: 
            (String) SKU: the SKU of the product to look for
            (String) PROPERTY: the property in search
        Returns: (Object) the corresponding value
        */
        
        Dictionary<String, Boolean> PROPERTY_EXISTS = new Hashtable<>();
        
        // Find the SKU. This isn't the most efficient method but will do for the purposes of lab. 
        Enumeration<String> items_SKU = items.keys();
        PROPERTY_EXISTS.put("SKU", false);
        do {
            String item_SKU = items_SKU.nextElement();
            PROPERTY_EXISTS.put("SKU", item_SKU.equals(SKU));
        } while (items_SKU.hasMoreElements() && (!(PROPERTY_EXISTS.get("SKU"))));
        
        // Stop when the SKU is not found. 
        if (!PROPERTY_EXISTS.get("SKU")) {
            return (null);
        };
        
        // Find the property within the element. 
        Enumeration<String> item_properties = items.get(SKU).keys();
        do {
            String item_property = item_properties.nextElement(); 
            PROPERTY_EXISTS.put("Property", item_property.equals(PROPERTY));
            if (PROPERTY_EXISTS.get("Property")) {
                break;
            };
        } while (item_properties.hasMoreElements());
        
        if (PROPERTY_EXISTS.get("Property")) {
            return (items.get(SKU)).get(PROPERTY);
        };
        
        return (null);
    };
    
    public static ArrayList<String> list() {
        /* List all inventory items.
        
        Returns: (ArrayList<String>) The array list
        */
        
        ArrayList<String> items_SKU = new ArrayList<>();
        Enumeration<String> items_SKU_raw = items.keys();
        while (items_SKU_raw.hasMoreElements()) {
            String item_SKU = items_SKU_raw.nextElement();
            items_SKU.add(item_SKU);
        };
        
        return(items_SKU);
    };
    
    public static ArrayList list(String PROPERTY) {
        ArrayList<Object> items_data = new ArrayList<>();
        ArrayList<String> items_SKU = list();
        
        for (String item_SKU : items_SKU) {
            Object item_data = "";
            try {
                item_data = items.get(item_SKU).get(PROPERTY);
            } catch (Exception e) {System.out.println("Exception occurred: " + e.getMessage());};
            
            if (item_data == null) {item_data = "";};
            items_data.add(item_data);
        };
        
        return(items_data);
    };
    
    public static ArrayList<String> collate() {
        /* Collate the names, replacing unknown names with their SKU. 
        
        Returns: (ArrayList<String>) the array list of names
        */
        
        ArrayList<String> items_names_display = list("Name");
        ArrayList<String> items_SKU = list();
        for (int item_rank = 0; item_rank < items_names_display.size(); item_rank++) {
            if (items_names_display.get(item_rank).isBlank()) {
                items_names_display.set(item_rank, items_SKU.get(item_rank));
            };
        };
        
        return(items_names_display);
    };
    
    public static Dictionary<String, Object> find(String NAME) {
        /* Find an item by its name, and get its information. 
        
        Parameters: 
            (String) NAME: the name or SKU of an item
        Returns: (Dictionary) its information
        */
        
        Dictionary<String, Object> SKU_DETAILS = null;
        
        refresh();
        Boolean isNAME = item_names.contains(NAME);
        Boolean isSKU = list().contains(NAME);
        
        if (isNAME) {
            int item_index = item_names.indexOf(NAME);
            SKU_DETAILS = items.get(list().get(item_index));
        } else if (isSKU) {
            SKU_DETAILS = items.get(NAME);
        };
        return (SKU_DETAILS);
    };
    
    public static Dictionary find(int INDEX) {
        /* Find an item by its index in the collated data, and get its information. 
        
        Parameters: 
            (String) NAME: the name or SKU of an item
        Returns: (Dictionary) its information
        */
        
        Dictionary SKU_DETAILS = null;
        
        refresh();
        int NAME_LENGTH = item_names.size();
        
        if (INDEX > NAME_LENGTH) {
            // Stop being out of bounds. 
            return (null); 
        };
        
        // Get the details of the data. 
        SKU_DETAILS = items.get(list().get(INDEX));
        
        // It's time to return the data. 
        return (SKU_DETAILS);
    };
    
    static {
        refresh();
        collate();
    }
};
