/*
  Hansly Saw
  CSCI
  Final Project
  February 12, 2024

  OnAlert: Alert GUI
*/

package tech.iBeans.POSware.Lite;

import java.util.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;

public class OnAlert extends JDialog {
	public static Dictionary<String, String> USER_INTERACTION = new Hashtable<>();

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField_alert_input;

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the dialog.
	 */
	public OnAlert(String MESSAGE_TITLE, String MESSAGE_BODY, int INPUT_OK_STATE, int INPUT_CANCEL_STATE, Boolean INPUT_TEXT) {
        setModal(true);
            
		// Set window size
		setSize(new Dimension(450, 200));
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		// apply window properties
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		// Set layout
		contentPanel.setLayout(new BorderLayout(0, 0));
		
		// Add items
		JLabel lblNewLabel_alert_text = new JLabel(); // change the content
		textField_alert_input = new JTextField();
		JPanel panel_actions = new JPanel();
		JButton button_confirm = new JButton(); // translate
		JButton button_cancel = new JButton(); // translate

		// Set layout
		contentPanel.add(lblNewLabel_alert_text, BorderLayout.NORTH);
		panel_actions.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(panel_actions, BorderLayout.SOUTH);

		// Display buttons when necessary
		switch(INPUT_OK_STATE) {
			case 2: 
				getRootPane().setDefaultButton(button_confirm);
			case 1: 
				button_confirm.setActionCommand("OK");
				panel_actions.add(button_confirm);
		};
		switch(INPUT_CANCEL_STATE) {
			case 2: 
				getRootPane().setDefaultButton(button_cancel);
			case 1: 
				button_cancel.setActionCommand("Cancel");
				panel_actions.add(button_cancel);
		};
		if (INPUT_TEXT) {
			contentPanel.add(textField_alert_input, BorderLayout.SOUTH);
			// textField_alert_input.setColumns(10);
		};

		// Set dynamic text
		setTitle(MESSAGE_TITLE);
		lblNewLabel_alert_text.setText(MESSAGE_BODY);
		button_confirm.setText("OK");
		button_cancel.setText("Cancel");
		
		// For the user response
		if (INPUT_TEXT) {
			USER_INTERACTION.put("Text Entered", "");
			
			DocumentListener user_interaction_text_record = new DocumentListener() {
				public void insertUpdate(DocumentEvent e) {
					user_interaction_text_record_edit();
				}
				public void removeUpdate(DocumentEvent e) {
					user_interaction_text_record_edit();
				}
				public void changedUpdate(DocumentEvent e) {
					user_interaction_text_record_edit();
				}
				public void user_interaction_text_record_edit() {
					USER_INTERACTION.put("Text Entered", textField_alert_input.getText());
				}
			};
			
			textField_alert_input.getDocument().addDocumentListener(user_interaction_text_record);
		};

		ActionListener user_interaction_click_primary = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USER_INTERACTION.put("Button Returned", "1");
				dispose();
			};
		}; 
		ActionListener user_interaction_click_cancel = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USER_INTERACTION.put("Button Returned", "0");
				dispose();
			};
		}; 
		
		addWindowListener(new WindowAdapter()
			{
				@Override
				public void windowClosing(WindowEvent e)
				{
					USER_INTERACTION.put("Button Returned", (INPUT_CANCEL_STATE >= 2) ? "0" : "1"); e.getWindow().dispose();	
				}
			});

		button_confirm.addActionListener(user_interaction_click_primary);
		button_cancel.addActionListener(user_interaction_click_cancel);
	}

}
