package tech.iBeans.POSware.Lite;

public class payment{
	public static Float total = null;
	public static Float change = null;
	public static Float in = null;
	public static boolean OK = false;
	public static Boolean state = null;

	public static void init(float TOTAL) {
		/* Set up the payment.  */
		change = (float) 0;
		in = (float) 0;
		total = TOTAL;
	};

	public static boolean calculate() {
		/* Calculate the change. 
		 * 
		 * Returns: (float) the transaction state
		 */

		OK = (total <= in); 
		if (OK) {change = in - total;}; // Do not calculate a smaller change. 
		return(OK);	
	};

	public static boolean calculate(float TOTAL, float IN) {
		/* Calculate the change from a provided total and input. 
		 * 
		 * Returns: (float) the calculated change
		 */

		total = TOTAL; 
		in = IN;

		return(calculate());
	};

	public static boolean calculate(float IN) {
		/* Calculate the change from a provided input. 
		 * 
		 * Returns: (float) the calculated change
		 */

		in = IN;

		return(calculate());
	};

	public static void finish() {
		/* Finalize the payment.  */
		payment.state = true;
		transact.finalise();
	};

};