/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package tech.iBeans.POSware.Lite;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author eleven
 */
public class OnPayment extends javax.swing.JDialog {

    /**
     * Creates new form Window_Payment
     */
    public OnPayment(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    private static OnPayment dialog_payment = new OnPayment(new javax.swing.JFrame(), true);

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        setModal(true);

        java.awt.GridBagConstraints gridBagConstraints;

        jPanel_information = new javax.swing.JPanel();
        jLabel_information_total = new javax.swing.JLabel();
        jTextField_information_total_value = new javax.swing.JTextField();
        jLabel_information_payment_value = new javax.swing.JLabel();
        jSpinner_information_payment_value = new javax.swing.JSpinner();
        jLabel_information_change = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jPanel_actions = new javax.swing.JPanel();
        jButton_action_cancel = new javax.swing.JButton();
        jButton_action_continue = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("POSware Lite: Payment");

        jPanel_information.setLayout(new java.awt.GridBagLayout());

        jLabel_information_total.setText("Total");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel_information.add(jLabel_information_total, gridBagConstraints);

        jTextField_information_total_value.setEditable(false);
        jTextField_information_total_value.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jTextField_information_total_value.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jTextField_information_total_value.setText("0");
        jTextField_information_total_value.setFocusable(false);
        jTextField_information_total_value.setMinimumSize(new java.awt.Dimension(30, 26));
        jTextField_information_total_value.setRequestFocusEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel_information.add(jTextField_information_total_value, gridBagConstraints);

        jLabel_information_payment_value.setText("Payment");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel_information.add(jLabel_information_payment_value, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jSpinner_information_payment_value.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                getCompletable();
            }
        });
        jSpinner_information_payment_value.setModel(model_price);
        jPanel_information.add(jSpinner_information_payment_value, gridBagConstraints);

        jLabel_information_change.setText("Change");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel_information.add(jLabel_information_change, gridBagConstraints);

        jTextField1.setEditable(false);
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel_information.add(jTextField1, gridBagConstraints);

        getContentPane().add(jPanel_information, java.awt.BorderLayout.CENTER);

        jButton_action_cancel.setText("Cancel");
        jButton_action_cancel.addActionListener(setCancelled);
        jPanel_actions.add(jButton_action_cancel);

        jButton_action_continue.setText("Complete");
        jButton_action_continue.addActionListener(setCompleted);
        jPanel_actions.add(jButton_action_continue);

        getContentPane().add(jPanel_actions, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JButton jButton_action_cancel;
    private static javax.swing.JButton jButton_action_continue;
    private static javax.swing.JLabel jLabel_information_change;
    private static javax.swing.JLabel jLabel_information_payment_value;
    private static javax.swing.JLabel jLabel_information_total;
    private static javax.swing.JPanel jPanel_actions;
    private static javax.swing.JPanel jPanel_information;
    private static javax.swing.JSpinner jSpinner_information_payment_value;
    private static javax.swing.JTextField jTextField1;
    private static javax.swing.JTextField jTextField_information_total_value;
    // End of variables declaration//GEN-END:variables
    
    public static Boolean display() {
		/* Display the transaction. */
		dialog_payment.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                // Reflect a cancelled payment. 
                payment.state = false;
                e.getWindow().dispose();
            }
        });
        getCompletable();
		dialog_payment.setVisible(true);
		return(payment.state);
	};

	// Set the numerical model. 
	private SpinnerNumberModel model_price = new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, .01);

	// Window events
	private static void getCompletable() {
		float in = Float.parseFloat(String.valueOf(jSpinner_information_payment_value.getValue()));
		
		// Calculate the value. 
		payment.calculate(in);
		setCompletable();
		update();
	}
	
	private static boolean setCompletable() {
		/* Calculate whether the change is sufficient. */
		setCompletable(payment.OK);
		
		return (payment.OK);
	};

	private static boolean setCompletable(boolean STATE) {
		/* Calculate whether the change is sufficient. */
		jButton_action_continue.setEnabled(STATE);

		if (STATE) {
			dialog_payment.getRootPane().setDefaultButton(jButton_action_continue);
		} else {
			dialog_payment.getRootPane().setDefaultButton(jButton_action_cancel);
		};

		return (STATE);
	};

	ActionListener setCompleted = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			payment.OK = true;
			payment.state = true;
			dispose();
            
            // Finish the payment. 
            payment.finish();
		};
	};

	ActionListener setCancelled = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			payment.OK = false;
			payment.state = false;
			dispose();
		};
	};

	
	// Update information. 
	public static void update() {
		/* Update the information. */
		if (payment.total != null) {
			jTextField_information_total_value.setText(String.valueOf(payment.total));
		};
		if (payment.change != null) {
			jTextField1.setText(String.valueOf(payment.change));
		};
	};

}
