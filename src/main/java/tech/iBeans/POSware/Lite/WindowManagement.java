/*
  Hansly Saw
  CSCI
  Final Project
  February 12, 2024

  Window Management
*/

package tech.iBeans.POSware.Lite;

import java.util.function.Function;

import javax.swing.JDialog;
import tech.iBeans.POSware.Lite.OnAlert.*;

/**
 *
 * @author tenth
 */
public class WindowManagement {
    public static void alert(String MESSAGE_TITLE, String MESSAGE_BODY, Boolean INVERT) {
        /* 
         * This displays an onscreen message, where the user interacts through one button.
         * 
         * Parameter: 
         * 	MESSAGE_TITLE (String): The message title
         *  MESSAGE_BODY (String): The message body
         * 	INVERT (Boolean): determines wheter show or cancel is default
         */

        try {
            OnAlert dialog = new OnAlert(MESSAGE_TITLE, MESSAGE_BODY, (!INVERT) ? 2 : 0, (INVERT) ? 2 : 0, false);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        };
    };

    public static void alert(String MESSAGE_TITLE, String MESSAGE_BODY) {
        /* 
         * This displays an onscreen message, where the user interacts through one button.
         * 
         * Parameter: 
         * 	MESSAGE_TITLE (String): The message title
         *  MESSAGE_BODY (String): The message body
         */

        alert(MESSAGE_TITLE, MESSAGE_BODY, false);
    };

    public static void alert(String MESSAGE_BODY) {
            /* 
             * This displays an onscreen message, where the user interacts through one button.
             * 
             * Parameter: 
             * 	MESSAGE_BODY (String): The message body
             */

            alert("", MESSAGE_BODY, false);
    };

    public static Boolean confirm(String MESSAGE_TITLE, String MESSAGE_BODY, Boolean INVERT) {
        /* 
         * This displays an onscreen message, where the user interacts through two buttons.
         * 
         * Parameter: 
         * 	MESSAGE_TITLE (String): The message title
         *  MESSAGE_BODY (String): The message body
         *  INVERT (Boolean): to set cancel as default
         */

        try {
               OnAlert dialog = new OnAlert(MESSAGE_TITLE, MESSAGE_BODY, 1+((!INVERT)? 1 : 0), 1+((INVERT)? 1 : 0), false);
               dialog.setVisible(true);
       } catch (Exception e) {
               e.printStackTrace();
       };

        return ((OnAlert.USER_INTERACTION.get("Button Returned").contains("1"))); 
    }

    public static Boolean confirm(String MESSAGE_TITLE, String MESSAGE_BODY) {
        /* 
         * This displays an onscreen message, where the user interacts through two buttons.
         * 
         * Parameter: 
         * 	MESSAGE_TITLE (String): The message title
         *  MESSAGE_BODY (String): The message body
         */

        
        return (confirm(MESSAGE_TITLE, MESSAGE_BODY, false)); 
    }

    public static Boolean confirm(String MESSAGE_BODY) {
            /* 
             * This displays an onscreen message, where the user interacts through two buttons.
             * 
             * Parameter: 
             * 	MESSAGE_BODY (String): The message body
             */

        return(confirm("", MESSAGE_BODY, false));
    }

    public static String input(String MESSAGE_TITLE, String MESSAGE_BODY) {
        /* 
         * This displays an onscreen message, where the user interacts through text.
         * 
         * Parameter: 
         * 	MESSAGE_TITLE (String): The message title
         *  MESSAGE_BODY (String): The message body
         */

         try {
            OnAlert dialog = new OnAlert(MESSAGE_TITLE, MESSAGE_BODY, 2, 0, true);
            dialog.setVisible(true);
        } catch (Exception e) {
                e.printStackTrace();
        };

        return (OnAlert.USER_INTERACTION.get("Text Entered"));
    }

    public static String input(String MESSAGE_BODY) {
        /* 
         * This displays an onscreen message, where the user interacts through text.
         * 
         * Parameter: 
         * 	MESSAGE_BODY (String): The message body
         */

         try {
            OnAlert dialog = new OnAlert("", MESSAGE_BODY, 2, 0, true);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        };

        return (OnAlert.USER_INTERACTION.get("Text Entered"));
    }
    
}
