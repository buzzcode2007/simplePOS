/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package tech.iBeans.POSware.Lite;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;

/**
 *
 * @author eleven
 */
public class OnTransact extends javax.swing.JFrame {

	/**
	 * Creates new form OnTransact
	 */
	private OnTransact() {
		initComponents();
	}

	
	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		jPanel_statusinfo = new javax.swing.JPanel();
		jLabel_Status = new javax.swing.JLabel();
		jToggleButton_Discard = new javax.swing.JToggleButton();
		jButton_Action_Pay = new javax.swing.JButton();
		jPanel_Main = new javax.swing.JPanel();
		jPanel_Items = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		jToolBar_Information = new javax.swing.JToolBar();
		jPanel_Information = new javax.swing.JPanel();
		jLabel_price_VAT = new javax.swing.JLabel();
		jLabel_price_tip = new javax.swing.JLabel();
		jLabel_price_subtotal = new javax.swing.JLabel();
		jLabel_price_total = new javax.swing.JLabel();
		jSpinner_price_tip_value = new javax.swing.JSpinner();
		jTextField_price_VAT_value = new javax.swing.JTextField();
		jTextField_price_subtotal_value = new javax.swing.JTextField();
		jTextField_price_total_value = new javax.swing.JTextField();
		jLabel2_information_discount = new javax.swing.JLabel();
		jTextField_price_discounted = new javax.swing.JTextField();
		jToolBar_Inventory = new javax.swing.JToolBar();
		jPanel_Inventory = new javax.swing.JPanel();
		jPanel_Inventory_Actions = new javax.swing.JPanel();
		jButton_item_void = new javax.swing.JButton();
		jSpinner_item_quantity = new javax.swing.JSpinner();
		jButton_item_add = new javax.swing.JButton();
		jScrollPane_Inventory = new javax.swing.JScrollPane();
		jList1_Inventory = new javax.swing.JList<>();
		jPanel_ItemDetails = new javax.swing.JPanel();
		jLabel_ItemDetails_SKU = new javax.swing.JLabel();
		jTextField_ItemDetails_SKU = new javax.swing.JTextField();
		jLabel_ItemDetails_UnitPrice = new javax.swing.JLabel();
		jTextField_ItemDetails_UnitPrice = new javax.swing.JTextField();
		jLabel_ItemDetails_Discount = new javax.swing.JLabel();
		jTextField_ItemDetails_Discount = new javax.swing.JTextField();

		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("POSware Lite: Transaction");
		setPreferredSize(new java.awt.Dimension(1000, 800));
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close_precheck(evt);
			}
		});

		jPanel_statusinfo.setLayout(new java.awt.BorderLayout());

		jLabel_Status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel_Status.setText("Store");
		jPanel_statusinfo.add(jLabel_Status, java.awt.BorderLayout.CENTER);

		jToggleButton_Discard.setText("Discard");
		jToggleButton_Discard.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jToggleButton_DiscardActionPerformed(evt);
			}
		});
		jPanel_statusinfo.add(jToggleButton_Discard, java.awt.BorderLayout.LINE_START);
		jToggleButton_Discard.setVisible(set_clear());

		jButton_Action_Pay.setText("Payment");
		jPanel_statusinfo.add(jButton_Action_Pay, java.awt.BorderLayout.LINE_END);

		getContentPane().add(jPanel_statusinfo, java.awt.BorderLayout.PAGE_END);

		jPanel_Main.setLayout(new java.awt.BorderLayout());

		jPanel_Items.setLayout(new java.awt.BorderLayout());

		jTextArea1.setColumns(20);
		jTextArea1.setLineWrap(true);
		jTextArea1.setRows(5);
		jTextArea1.setEditable(false);
		jScrollPane1.setViewportView(jTextArea1);

		jPanel_Items.add(jScrollPane1, java.awt.BorderLayout.CENTER);

		jToolBar_Information.setRollover(true);

		jPanel_Information.setLayout(new java.awt.GridBagLayout());

		jLabel_price_VAT.setText("Tax");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		jPanel_Information.add(jLabel_price_VAT, gridBagConstraints);

		jLabel_price_tip.setText("Tip");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		jPanel_Information.add(jLabel_price_tip, gridBagConstraints);

		jLabel_price_subtotal.setText("Subtotal");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		jPanel_Information.add(jLabel_price_subtotal, gridBagConstraints);

		jLabel_price_total.setText("Grand Total");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridheight = 2;
		jPanel_Information.add(jLabel_price_total, gridBagConstraints);

		jSpinner_price_tip_value.setModel(model_price);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		jPanel_Information.add(jSpinner_price_tip_value, gridBagConstraints);
		jSpinner_price_tip_value.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				cart_add_tip();
			}
		});

		jTextField_price_VAT_value.setEditable(false);
		jTextField_price_VAT_value.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		jPanel_Information.add(jTextField_price_VAT_value, gridBagConstraints);

		jTextField_price_subtotal_value.setEditable(false);
		jTextField_price_subtotal_value.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		jTextField_price_subtotal_value.setText("0");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		jPanel_Information.add(jTextField_price_subtotal_value, gridBagConstraints);

		jTextField_price_total_value.setEditable(false);
		jTextField_price_total_value.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N
		jTextField_price_total_value.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		jTextField_price_total_value.setText("0");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridheight = 2;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		jPanel_Information.add(jTextField_price_total_value, gridBagConstraints);

		jLabel2_information_discount.setText("Discount");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		jPanel_Information.add(jLabel2_information_discount, gridBagConstraints);

		jTextField_price_discounted.setEditable(false);
		jTextField_price_discounted.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		jPanel_Information.add(jTextField_price_discounted, gridBagConstraints);

		jToolBar_Information.add(jPanel_Information);

		jPanel_Items.add(jToolBar_Information, java.awt.BorderLayout.SOUTH);

		jPanel_Main.add(jPanel_Items, java.awt.BorderLayout.CENTER);

		jToolBar_Inventory.setRollover(true);

		jPanel_Inventory.setLayout(new java.awt.GridBagLayout());

		jPanel_Inventory_Actions.setLayout(new java.awt.BorderLayout());

		jButton_item_void.setText("Void");
		jButton_item_void.setFocusable(false);
		jButton_item_void.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		jButton_item_void.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		jButton_item_void.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton_item_voidActionPerformed(evt);
			}
		});
		jPanel_Inventory_Actions.add(jButton_item_void, java.awt.BorderLayout.LINE_START);

		jSpinner_item_quantity.setModel(model_counting);
		jPanel_Inventory_Actions.add(jSpinner_item_quantity, java.awt.BorderLayout.CENTER);

		jButton_item_add.setText("Add");
		jButton_item_add.setFocusable(false);
		jButton_item_add.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		jButton_item_add.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		jButton_item_add.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton_item_addActionPerformed(evt);
			}
		});
		jPanel_Inventory_Actions.add(jButton_item_add, java.awt.BorderLayout.LINE_END);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		
		jList1_Inventory.setModel(refresh_inventory_list());
		jList1_Inventory.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				jList1_InventoryValueChanged(evt);
			}
		});
		
		jPanel_Inventory.add(jPanel_Inventory_Actions, gridBagConstraints);
		jScrollPane_Inventory.setViewportView(jList1_Inventory);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		jPanel_Inventory.add(jScrollPane_Inventory, gridBagConstraints);

		jPanel_ItemDetails.setLayout(new java.awt.GridBagLayout());

		jLabel_ItemDetails_SKU.setText("SKU");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		jPanel_ItemDetails.add(jLabel_ItemDetails_SKU, gridBagConstraints);

		jTextField_ItemDetails_SKU.setEditable(false);
		jTextField_ItemDetails_SKU.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		jPanel_ItemDetails.add(jTextField_ItemDetails_SKU, gridBagConstraints);

		jLabel_ItemDetails_UnitPrice.setText("Unit Price");
		jLabel_ItemDetails_UnitPrice.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		jPanel_ItemDetails.add(jLabel_ItemDetails_UnitPrice, gridBagConstraints);

		jTextField_ItemDetails_UnitPrice.setEditable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		jPanel_ItemDetails.add(jTextField_ItemDetails_UnitPrice, gridBagConstraints);

		jLabel_ItemDetails_Discount.setText("Discount");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		jPanel_ItemDetails.add(jLabel_ItemDetails_Discount, gridBagConstraints);

		jTextField_ItemDetails_Discount.setEditable(false);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		jPanel_ItemDetails.add(jTextField_ItemDetails_Discount, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		jPanel_Inventory.add(jPanel_ItemDetails, gridBagConstraints);

		jToolBar_Inventory.add(jPanel_Inventory);

		jPanel_Main.add(jToolBar_Inventory, java.awt.BorderLayout.EAST);

		getContentPane().add(jPanel_Main, java.awt.BorderLayout.CENTER);

		// Add action listeners. 
		jButton_Action_Pay.addActionListener(select_payment);

		pack();
	}// </editor-fold>

	// Window events
	private void close_precheck(java.awt.event.WindowEvent evt) {
		boolean CLOSING_STATE = receipt.clear; 
		if (!receipt.clear) {CLOSING_STATE = reset();};

		if (CLOSING_STATE) {evt.getWindow().dispose();};
	}

	private void jList1_InventoryValueChanged(javax.swing.event.ListSelectionEvent evt) {
		product_information_refresh();
	}

	private boolean jToggleButton_DiscardActionPerformed(java.awt.event.ActionEvent evt) {
		// When the discard button is clicked
		boolean user_choice = reset();
		jToggleButton_Discard.setSelected(false);
		return (user_choice);
	};

	/**
	 * @param args the command line arguments
	 */
	
	public static void display() {
		/* Display the transaction. */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new OnTransact().setVisible(true);
				init();
			}
		});
	}
	
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(OnTransact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(OnTransact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(OnTransact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(OnTransact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		display();
	}
	
	private static void init() {
		transact.init();
		refresh_inventory_list();
		refresh_data();
		refresh_interface();
	};
	
	public static Boolean reset() {
		/* Reset the transaction. */
		Boolean DISCARD_STATE = (!transact.progress || confirm_discard());
		if (DISCARD_STATE) {
			init();
		};
		
		return (DISCARD_STATE);
	};

	private SpinnerNumberModel model_price = new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, .01);
	private SpinnerNumberModel model_counting = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);

	
	// Adjusting the interface
	private static void refresh_interface() {
		set_discard();
		set_clear();
		set_payment();
		set_modifiable();
	};

	private static boolean set_clear() {
		/* Adjust the ability to reset. 
		
		Returns: (boolean) the new state of the appearance
		*/
		
		boolean CLEARABLE_STATE = (!receipt.clear); 
		jToggleButton_Discard.setVisible(CLEARABLE_STATE);
		return (jToggleButton_Discard.isVisible());
	};
	
	public static boolean set_payment() {
		/* Adjust the ability to pay. 
		
		Returns: (boolean) the payment status */
		
		// Can only pay when the transaction is currently active
		jButton_Action_Pay.setEnabled(transact.check() && transact.progress);
		return(transact.check());
	}

	private static boolean set_discard() {
		jToggleButton_Discard.setEnabled(!receipt.clear);
		jToggleButton_Discard.setText((!transact.progress) ? "Clear" : "Discard");
		return(!receipt.clear);
	};

	private static boolean set_modifiable() {
		jSpinner_price_tip_value.setEnabled(transact.progress);

		return(transact.progress);
	}
	
	// The discarding of data
	private static boolean confirm_discard() {
		/* Confirm discarding items. 
		
		Returns: 
			(boolean) the user's selection
		 */
		
		Boolean USER_CHOICE = WindowManagement.confirm("Discarding Items", "There are currently items in the cart. Would you want to discard this?", true); 
		return (USER_CHOICE);
	};

	// Inventories
	private static AbstractListModel<String> refresh_inventory_list() {
		/* Pull the list from inventory. 
		*/
		
		AbstractListModel<String> INVENTORY_LIST = new javax.swing.AbstractListModel<String>() {
			String[] strings = ((ArrayList<String>)inventory.collate()).toArray(new String[0]);
			public int getSize() { return strings.length; }
			public String getElementAt(int i) { return strings[i]; }
		};
		
		return(INVENTORY_LIST);
	};
	
	//# Refreshing of data
	private static void refresh_data() {
		cart_calculation_refresh();
		product_information_refresh();
		receipt_refresh();
	};

	private static void cart_calculation_refresh() {
		/* Refresh the cart's details, such as the total cost. */
		Dictionary<String, Float> prices = transact.calculate(); 
		
		jTextField_price_subtotal_value.setText(String.valueOf(prices.get("subtotal")));
		jTextField_price_total_value.setText(String.valueOf(prices.get("total")));
		jTextField_price_discounted.setText(String.valueOf(prices.get("discount")));
		jTextField_price_VAT_value.setText(String.valueOf(prices.get("tax")));
		
		set_payment();
		set_clear();
	};
	
	private static boolean product_information_refresh() {
		/* Refresh the select product information's details and the corresponding button states. */
		Integer ITEM_SELECTED = jList1_Inventory.getSelectedIndex();
		
		// Set the initial values. 
		jSpinner_item_quantity.setEnabled((ITEM_SELECTED >= 0 && transact.progress));
		jButton_item_void.setEnabled((ITEM_SELECTED >= 0 && transact.progress)); 
		jButton_item_add.setEnabled((ITEM_SELECTED >= 0 && transact.progress)); 
		
		if (ITEM_SELECTED < 0) {
			// Stop when there is no value selected. 
			return(false);
		};
		
		Dictionary<String, String> ITEM_SELECTED_DETAILS_PROCESSED = new Hashtable<>();
		
		// Get the selected details. 
		@SuppressWarnings("unchecked")
		Dictionary<String, Object> ITEM_SELECTED_DETAILS = inventory.find(ITEM_SELECTED);
		if (ITEM_SELECTED_DETAILS == null) {return (false);};
		ITEM_SELECTED_DETAILS_PROCESSED.put("SKU", inventory.list().get(ITEM_SELECTED).toString());
		
		// Get the unit price. 
		ITEM_SELECTED_DETAILS_PROCESSED.put("Unit Price", String.valueOf(ITEM_SELECTED_DETAILS.get("Price")));
		
		// Get the discount. 
		if (ITEM_SELECTED_DETAILS.get("Discount") != null) {
			if ((int) ITEM_SELECTED_DETAILS.get("Discount") != 0) {
				ITEM_SELECTED_DETAILS_PROCESSED.put("Discount", (String.valueOf((int) ITEM_SELECTED_DETAILS.get("Discount"))).concat("%"));
			};
		} else {
			ITEM_SELECTED_DETAILS_PROCESSED.put("Discount","");
		};
		
		// Change the void state. You can't void something that doesn't already exist in the cart. 
		jButton_item_void.setEnabled((((transact.items).get(ITEM_SELECTED_DETAILS_PROCESSED.get("SKU"))) != null) && transact.progress);

		// Also stop the user from accidentally selecting the wrong amount
		jSpinner_item_quantity.setValue(1);
		
		// Place the corresponding information. 
		jTextField_ItemDetails_SKU.setText(ITEM_SELECTED_DETAILS_PROCESSED.get("SKU"));
		jTextField_ItemDetails_UnitPrice.setText(ITEM_SELECTED_DETAILS_PROCESSED.get("Unit Price"));
		jTextField_ItemDetails_Discount.setText(ITEM_SELECTED_DETAILS_PROCESSED.get("Discount"));
		
		return (true);
	};
	
	private static String receipt_refresh() {
		receipt.refresh();
		jTextArea1.setText(receipt.content);
		return(receipt.content);
	};
	
	// Cart items
	private void jButton_item_voidActionPerformed(java.awt.event.ActionEvent evt) {
		cart_remove();
	}

	private void jButton_item_addActionPerformed(java.awt.event.ActionEvent evt) {
		cart_add();
	}

	@SuppressWarnings("unchecked")
	private boolean cart_add() {
		/* Add the item to the cart. 
		
		Returns: (boolean) whether the item had been successfully added to the cart or not
		*/
		boolean state = false; 
		
		int ITEM_SELECTED = jList1_Inventory.getSelectedIndex();
		int ITEM_SELECTED_QUANTITY = Integer.parseInt(String.valueOf(jSpinner_item_quantity.getValue()));
		
		if (ITEM_SELECTED >= 0) {
			// Get the selected details. 
			Dictionary<String, Object> ITEM_SELECTED_DETAILS = inventory.find(ITEM_SELECTED);
			if (ITEM_SELECTED_DETAILS == null) {return (false);};
			String ITEM_SELECTED_SKU = inventory.list().get(ITEM_SELECTED).toString();

			// Add to cart. 
			state = transact.add(ITEM_SELECTED_SKU, ITEM_SELECTED_QUANTITY);

			// Refresh the data. 
			refresh_data();
			refresh_interface();
		};
		return(state);
	};

	private void cart_add_tip() {
		/* Add a tip. */
		
		transact.price.put("tip", Float.valueOf(String.valueOf(jSpinner_price_tip_value.getValue())));
		transact.calculate();
		refresh_data();
	};
	
	@SuppressWarnings("unchecked")
	private boolean cart_remove() {
		/* Remove the item to the cart. 
		
		Returns: (boolean) whether the item had been successfully removed from the cart or not
		*/
		boolean state = false; 
		
		int ITEM_SELECTED = jList1_Inventory.getSelectedIndex();
		int ITEM_SELECTED_QUANTITY = (int) jSpinner_item_quantity.getValue();
		
		if (ITEM_SELECTED >= 0) {
			// Get the selected details. 
			Dictionary<String, Object> ITEM_SELECTED_DETAILS = inventory.find(ITEM_SELECTED);
			if (ITEM_SELECTED_DETAILS == null) {return (false);};
			String ITEM_SELECTED_SKU = inventory.list().get(ITEM_SELECTED).toString();

			// Add to cart. 
			state = transact.remove(ITEM_SELECTED_SKU, ITEM_SELECTED_QUANTITY);

			// Refresh the data. 
			refresh_data();
			refresh_interface();
		};
		return(state);
	};
	
	// Action events. 
	ActionListener select_payment = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			transact.pay();
			OnPayment.display();
			refresh_data();
			refresh_interface();
		};
	};
	

	// Variables declaration - do not modify
	private static javax.swing.JButton jButton_Action_Pay;
	private static javax.swing.JButton jButton_item_add;
	private static javax.swing.JButton jButton_item_void;
	private static javax.swing.JLabel jLabel2_information_discount;
	private static javax.swing.JLabel jLabel_ItemDetails_Discount;
	private static javax.swing.JLabel jLabel_ItemDetails_SKU;
	private static javax.swing.JLabel jLabel_ItemDetails_UnitPrice;
	private static javax.swing.JLabel jLabel_Status;
	private static javax.swing.JLabel jLabel_price_VAT;
	private static javax.swing.JLabel jLabel_price_subtotal;
	private static javax.swing.JLabel jLabel_price_tip;
	private static javax.swing.JLabel jLabel_price_total;
	private static javax.swing.JList<String> jList1_Inventory;
	private static javax.swing.JPanel jPanel_Information;
	private static javax.swing.JPanel jPanel_Inventory;
	private static javax.swing.JPanel jPanel_Inventory_Actions;
	private static javax.swing.JPanel jPanel_ItemDetails;
	private static javax.swing.JPanel jPanel_Items;
	private static javax.swing.JPanel jPanel_Main;
	private static javax.swing.JPanel jPanel_statusinfo;
	private static javax.swing.JScrollPane jScrollPane1;
	private static javax.swing.JScrollPane jScrollPane_Inventory;
	private static javax.swing.JSpinner jSpinner_item_quantity;
	private static javax.swing.JSpinner jSpinner_price_tip_value;
	private static javax.swing.JTextArea jTextArea1;
	private static javax.swing.JTextField jTextField_ItemDetails_Discount;
	private static javax.swing.JTextField jTextField_ItemDetails_SKU;
	private static javax.swing.JTextField jTextField_ItemDetails_UnitPrice;
	private static javax.swing.JTextField jTextField_price_VAT_value;
	private static javax.swing.JTextField jTextField_price_discounted;
	private static javax.swing.JTextField jTextField_price_subtotal_value;
	private static javax.swing.JTextField jTextField_price_total_value;
	private static javax.swing.JToggleButton jToggleButton_Discard;
	private static javax.swing.JToolBar jToolBar_Information;
	private static javax.swing.JToolBar jToolBar_Inventory;
	// End of variables declaration
}
