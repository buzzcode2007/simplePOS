/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tech.iBeans.POSware.Lite;


import java.text.DecimalFormat;
// Import global modules. 
import java.util.*;

/**
 *
 * @author eleven
 */
public class transact {
    public static Dictionary<String, Dictionary<String, Object>> items = new Hashtable<>(); 
    public static Dictionary<String, Float> price = new Hashtable<>();
	public static Boolean state = false;
    public static Boolean progress = false;
    
    /* 
    {item: {
        qty: 
        price: {
            "tax": 
            "subtotal": 
            "total": 
            "discount": 
        }
    }
    }
    */

    private final static DecimalFormat FORMAT_DECIMAL = new DecimalFormat("#.00");
    
    
    public static void init() {
        /* Initialize the variables, setting them with default values. 
        */
        
        progress = true;
        items = new Hashtable<>();
        price = new Hashtable<>();
        
        // Set default values for prices. 
        price.put("total", (float) 0.00);
        price.put("tax", (float) 0.00);
        price.put("subtotal", (float) 0.00);
        price.put("discount", (float) 0.00);
		
		// Initialize as well the receipt. 
		receipt.init();
		receipt.create();
    };
    
	public static Boolean check() {
		/* Check the status of the transaction. 
		
		Returns: (Boolean) the transaction state */
		
		state = !items.isEmpty();
		
		// Return the state. 
		return(state);
	}
	
    @SuppressWarnings("unchecked")
    public static Dictionary<String, Float> calculate() {
        /* Calculate the subtotal, total, and VAT. 
        
        Returns: (Dictionary) the contents of the price variable
        */
        
        // Prepare to enumerate each item. 
        Enumeration<String> items_list = items.keys();
        
        // Set default values for prices. 
        price.put("total", (float) 0.00);
        price.put("subtotal", (float) 0.00);
        price.put("discount", (float) 0.00);
        price.put("tax", (float) 0.00);
        
        while (items_list.hasMoreElements()) {
            String item = items_list.nextElement();
            
            if (((Dictionary<String, Object>) items.get(item).get("price")).get("tax") != null) {
                price.put("tax", (float) price.get("tax") + (float) ((Dictionary<String, Object>) items.get(item).get("price")).get("tax"));
            };
            if (((Dictionary<String, Object>) items.get(item).get("price")).get("subtotal") != null) {
                price.put("subtotal", (float) price.get("subtotal") + (float) ((Dictionary<String, Object>) items.get(item).get("price")).get("subtotal"));
            };
            if (((Dictionary<String, Object>) items.get(item).get("price")).get("total") != null) {
                price.put("total", (float) price.get("total") + (float) ((Dictionary<String, Object>) items.get(item).get("price")).get("total"));
            };
            if (((Dictionary<String, Object>) items.get(item).get("price")).get("discount") != null) {
                price.put("discount", (float) price.get("discount") + (float) ((Dictionary<String, Object>) items.get(item).get("price")).get("discount"));
            };
        };
        
        if (price.get("tip") != null) {
            if (Float.parseFloat(price.get("tip").toString()) >= 0) {
                price.put("total", (float) price.get("total") + (float) price.get("tip"));
            };
        };
        
        // Return the dictionary. 
        return(price);
    };
    
    @SuppressWarnings("unchecked")
    public static Dictionary<String, Float> calculate(String SKU, int QUANTITY) {
        inventory.refresh();
        Dictionary<String, Object> item_details = inventory.items.get(SKU);
        
        // Create an output dictionary. 
        Dictionary<String, Float> item_pricing = new Hashtable<>();
        
        // Fill in with default data, especially useful when the product is not found. 
        item_pricing.put("subtotal", (float) 0.00);
        item_pricing.put("total", (float) 0.00);
        
        if (item_details != null) {
            if (item_details.get("Price") != null) {
                // Set up the decimal format. 

                item_pricing.put("unit", Float.valueOf(FORMAT_DECIMAL.format(Float.valueOf(item_details.get("Price").toString()))));
                item_pricing.put("subtotal", Float.valueOf(FORMAT_DECIMAL.format(Float.parseFloat(item_details.get("Price").toString()) * QUANTITY)));
                item_pricing.put("total", Float.valueOf(FORMAT_DECIMAL.format(item_pricing.get("subtotal"))));
                
                if (item_details.get("Tax") != null) {
                    item_pricing.put("tax", Float.valueOf(FORMAT_DECIMAL.format(Float.valueOf(item_details.get("Tax").toString()) * QUANTITY)));
                    item_pricing.put("total", Float.valueOf(item_pricing.get("total").toString()) + Float.valueOf(item_pricing.get("tax").toString()));
                };
                if (item_details.get("Discount") != null) {
                    if (Integer.parseInt(item_details.get("Discount").toString()) > 0) {
                        item_pricing.put("discount", Float.valueOf(FORMAT_DECIMAL.format(
                            QUANTITY * Float.parseFloat(item_details.get("Price").toString()) * (1 - (Float.parseFloat(item_details.get("Discount").toString()) / 100))
                            )));
                    } else if (Integer.parseInt(item_details.get("Discount").toString()) < 0) {
                        item_pricing.put("discount", Float.valueOf(FORMAT_DECIMAL.format(
                                QUANTITY * Float.parseFloat(item_details.get("Price").toString()) * (0 - (Float.parseFloat(item_details.get("Discount").toString()) / 100))
                        )));
                    } else {
                        item_pricing.put("discount", (float) 0);
                    };
                    
                    item_pricing.put("total", Float.valueOf(FORMAT_DECIMAL.format(Float.valueOf(item_pricing.get("total").toString()) - Float.valueOf(item_pricing.get("discount").toString()))));
                };
            };
        };
        
        // Return the data. 
        return item_pricing;
        
    };
    
    private static void log(String SKU, int QUANTITY) {
        /* Log the change in the cart. 
        
        Parameters: 
            (String) SKU: the sku to look for
            (int) QUANTITY: the quantity of the product added or removed
        */
        
        var action_pricing = calculate(SKU, QUANTITY);
        receipt.action(inventory.collate().get(inventory.list().indexOf(SKU)), QUANTITY, action_pricing.get("unit"), action_pricing.get("total"));
    }; 
    
    public static boolean add(String SKU, int QUANTITY) {
        /* Add an item to the shopping cart. 
        Parameters: 
            SKU (String): the SKU of the item
            QUANTITY (int): the quantity
        Returns: (boolean) the calculation state
        */
        
        if (QUANTITY == 0 || !progress) {return(false);}
        else if (QUANTITY < 0) {return(remove(SKU, QUANTITY));};
        
        // containing the current data's information
        Dictionary<String, Object> ITEM_CURRENT_DATA = items.get(SKU);
        
        // check if the item exists
        if (ITEM_CURRENT_DATA != null) {
            ITEM_CURRENT_DATA.put("qty", ((int) ITEM_CURRENT_DATA.get("qty")) + QUANTITY);
        } else {
            // Initialize the dictionary. 
            ITEM_CURRENT_DATA = new Hashtable<>();
            
            // Add the quantity. 
            ITEM_CURRENT_DATA.put("qty", QUANTITY);
        };
        
        // Calculate these items' price. 
        ITEM_CURRENT_DATA.put("price", calculate(SKU, ((int) ITEM_CURRENT_DATA.get("qty"))));
        
        // Add to the shopping cart's items. 
        items.put(SKU, ITEM_CURRENT_DATA);
        
        // Calculate all prices. 
        calculate();
        
        // Add to the receipt when successful. 
        if ((items.get(SKU) != null)) {
            log(SKU, QUANTITY);
        };
        
        // Return the success state. 
        return((items.get(SKU) != null));
    };
    
    public static boolean remove(String SKU, int QUANTITY) {
        /* Remove an item to the shopping items. 
        
        Parameters: 
            SKU (String): the SKU of the item
            QUANTITY (int): the quantity
        Returns: (boolean) the removal state
        */
        
        if (QUANTITY == 0 || !progress) {return(false);}
        else if (QUANTITY < 0) {return(add(SKU, QUANTITY));};
        
        // containing the current data's information
        Dictionary<String, Object> ITEM_CURRENT_DATA = items.get(SKU);
        
        // check if the item exists. There's no need to remove an item that's already been removed. 
        if (ITEM_CURRENT_DATA != null) {
            if (QUANTITY >= (int) ITEM_CURRENT_DATA.get("qty")) {
                items.remove(SKU);
				
	            // Log the details. 
	            log(SKU, -((int) ITEM_CURRENT_DATA.get("qty")));
            } else {
                ITEM_CURRENT_DATA.put("qty", ((int) ITEM_CURRENT_DATA.get("qty")) - QUANTITY);
                ITEM_CURRENT_DATA.put("price", calculate(SKU, ((int) ITEM_CURRENT_DATA.get("qty"))));
                
                // Add to the shopping cart's items. 
                items.put(SKU, ITEM_CURRENT_DATA);
				
	            // Log the details. 
	            log(SKU, -QUANTITY);
            };
            
            // Calculate all prices. 
            calculate();
        };
        
        // Return the state. 
        return(true);
    };

    public static void pay() {
        /* Initialize the payment process of the transaction.  */
        if (state && progress) {
            // Can only run when the transaction is active (you don't want to pay again right after you've paid, right?)
            payment.init(Float.valueOf(FORMAT_DECIMAL.format(Float.valueOf(price.get("total").toString()))));
        };
    };

    public static void finalise() {
        /* Finalize the transaction. */

        // Can only be run if the payment has been confirmed
        if (payment.state != null) {
            if (payment.OK && payment.state) {
                progress = false; // no more modifications
                price.put("change", payment.change);
                receipt.payment(price);
            };
        };

        receipt.finish();
    };
};
